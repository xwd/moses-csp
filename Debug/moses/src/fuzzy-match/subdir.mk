################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../moses/src/fuzzy-match/FuzzyMatchWrapper.cpp \
../moses/src/fuzzy-match/SentenceAlignment.cpp \
../moses/src/fuzzy-match/SuffixArray.cpp \
../moses/src/fuzzy-match/Vocabulary.cpp 

OBJS += \
./moses/src/fuzzy-match/FuzzyMatchWrapper.o \
./moses/src/fuzzy-match/SentenceAlignment.o \
./moses/src/fuzzy-match/SuffixArray.o \
./moses/src/fuzzy-match/Vocabulary.o 

CPP_DEPS += \
./moses/src/fuzzy-match/FuzzyMatchWrapper.d \
./moses/src/fuzzy-match/SentenceAlignment.d \
./moses/src/fuzzy-match/SuffixArray.d \
./moses/src/fuzzy-match/Vocabulary.d 


# Each subdirectory must supply rules for building sources it contributes
moses/src/fuzzy-match/%.o: ../moses/src/fuzzy-match/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


