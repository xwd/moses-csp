################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../moses/src/CompactPT/BlockHashIndex.cpp \
../moses/src/CompactPT/CmphStringVectorAdapter.cpp \
../moses/src/CompactPT/LexicalReorderingTableCompact.cpp \
../moses/src/CompactPT/LexicalReorderingTableCreator.cpp \
../moses/src/CompactPT/MurmurHash3.cpp \
../moses/src/CompactPT/PhraseDecoder.cpp \
../moses/src/CompactPT/PhraseDictionaryCompact.cpp \
../moses/src/CompactPT/PhraseTableCreator.cpp \
../moses/src/CompactPT/ThrowingFwrite.cpp 

OBJS += \
./moses/src/CompactPT/BlockHashIndex.o \
./moses/src/CompactPT/CmphStringVectorAdapter.o \
./moses/src/CompactPT/LexicalReorderingTableCompact.o \
./moses/src/CompactPT/LexicalReorderingTableCreator.o \
./moses/src/CompactPT/MurmurHash3.o \
./moses/src/CompactPT/PhraseDecoder.o \
./moses/src/CompactPT/PhraseDictionaryCompact.o \
./moses/src/CompactPT/PhraseTableCreator.o \
./moses/src/CompactPT/ThrowingFwrite.o 

CPP_DEPS += \
./moses/src/CompactPT/BlockHashIndex.d \
./moses/src/CompactPT/CmphStringVectorAdapter.d \
./moses/src/CompactPT/LexicalReorderingTableCompact.d \
./moses/src/CompactPT/LexicalReorderingTableCreator.d \
./moses/src/CompactPT/MurmurHash3.d \
./moses/src/CompactPT/PhraseDecoder.d \
./moses/src/CompactPT/PhraseDictionaryCompact.d \
./moses/src/CompactPT/PhraseTableCreator.d \
./moses/src/CompactPT/ThrowingFwrite.d 


# Each subdirectory must supply rules for building sources it contributes
moses/src/CompactPT/%.o: ../moses/src/CompactPT/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


