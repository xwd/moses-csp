################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../phrase-extract/AlignmentPhrase.cpp \
../phrase-extract/ExtractedRule.cpp \
../phrase-extract/HoleCollection.cpp \
../phrase-extract/InputFileStream.cpp \
../phrase-extract/OutputFileStream.cpp \
../phrase-extract/PhraseAlignment.cpp \
../phrase-extract/SentenceAlignment.cpp \
../phrase-extract/SentenceAlignmentWithSyntax.cpp \
../phrase-extract/SyntaxTree.cpp \
../phrase-extract/XmlTree.cpp \
../phrase-extract/consolidate-direct.cpp \
../phrase-extract/consolidate-reverse.cpp \
../phrase-extract/consolidate.cpp \
../phrase-extract/domain.cpp \
../phrase-extract/extract-lex.cpp \
../phrase-extract/extract-rules.cpp \
../phrase-extract/extract.cpp \
../phrase-extract/relax-parse.cpp \
../phrase-extract/score.cpp \
../phrase-extract/statistics.cpp \
../phrase-extract/tables-core.cpp 

OBJS += \
./phrase-extract/AlignmentPhrase.o \
./phrase-extract/ExtractedRule.o \
./phrase-extract/HoleCollection.o \
./phrase-extract/InputFileStream.o \
./phrase-extract/OutputFileStream.o \
./phrase-extract/PhraseAlignment.o \
./phrase-extract/SentenceAlignment.o \
./phrase-extract/SentenceAlignmentWithSyntax.o \
./phrase-extract/SyntaxTree.o \
./phrase-extract/XmlTree.o \
./phrase-extract/consolidate-direct.o \
./phrase-extract/consolidate-reverse.o \
./phrase-extract/consolidate.o \
./phrase-extract/domain.o \
./phrase-extract/extract-lex.o \
./phrase-extract/extract-rules.o \
./phrase-extract/extract.o \
./phrase-extract/relax-parse.o \
./phrase-extract/score.o \
./phrase-extract/statistics.o \
./phrase-extract/tables-core.o 

CPP_DEPS += \
./phrase-extract/AlignmentPhrase.d \
./phrase-extract/ExtractedRule.d \
./phrase-extract/HoleCollection.d \
./phrase-extract/InputFileStream.d \
./phrase-extract/OutputFileStream.d \
./phrase-extract/PhraseAlignment.d \
./phrase-extract/SentenceAlignment.d \
./phrase-extract/SentenceAlignmentWithSyntax.d \
./phrase-extract/SyntaxTree.d \
./phrase-extract/XmlTree.d \
./phrase-extract/consolidate-direct.d \
./phrase-extract/consolidate-reverse.d \
./phrase-extract/consolidate.d \
./phrase-extract/domain.d \
./phrase-extract/extract-lex.d \
./phrase-extract/extract-rules.d \
./phrase-extract/extract.d \
./phrase-extract/relax-parse.d \
./phrase-extract/score.d \
./phrase-extract/statistics.d \
./phrase-extract/tables-core.d 


# Each subdirectory must supply rules for building sources it contributes
phrase-extract/%.o: ../phrase-extract/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


