################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../contrib/fuzzy-match/SuffixArray.cpp \
../contrib/fuzzy-match/Util.cpp \
../contrib/fuzzy-match/Vocabulary.cpp \
../contrib/fuzzy-match/fuzzy-match2.cpp \
../contrib/fuzzy-match/suffix-test.cpp 

OBJS += \
./contrib/fuzzy-match/SuffixArray.o \
./contrib/fuzzy-match/Util.o \
./contrib/fuzzy-match/Vocabulary.o \
./contrib/fuzzy-match/fuzzy-match2.o \
./contrib/fuzzy-match/suffix-test.o 

CPP_DEPS += \
./contrib/fuzzy-match/SuffixArray.d \
./contrib/fuzzy-match/Util.d \
./contrib/fuzzy-match/Vocabulary.d \
./contrib/fuzzy-match/fuzzy-match2.d \
./contrib/fuzzy-match/suffix-test.d 


# Each subdirectory must supply rules for building sources it contributes
contrib/fuzzy-match/%.o: ../contrib/fuzzy-match/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


